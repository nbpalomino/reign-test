Reign Test Code
=
*A test code for Reign by Nick B. Palomino*

Frontend
-
![image](frontend/assets/screenshot.png)

Backend
-
![image](backend/screenshot.png)

Configuration
-
- Run `docker-compose up` to start containers.
- Run `curl localhost:3000/posts/load` to provision DB schemas.
- In browser go to `http://localhost:8080/` to see this project in action.