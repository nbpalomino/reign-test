import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PostModule } from './posts/post.module';
import { ScheduleModule } from '@nestjs/schedule';
//import main from './config/main'

@Module({
  imports: [    
    ConfigModule.forRoot({ isGlobal: true }),
    ScheduleModule.forRoot(),
    PostModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
