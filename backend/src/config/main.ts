export default () => ({
  user: process.env.MONGO_USERNAME,
  pass: process.env.MONGO_PASSWORD,
  server: process.env.MONGO_SERVER,
  database: process.env.MONGO_DATABASE,
})