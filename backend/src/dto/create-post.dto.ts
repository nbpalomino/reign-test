export class CreatePostDto {    

    constructor(
        title: string,
        url: string,
        author: string,
        created_at: string,
        story_id: number,
        status: boolean) {
      this.title = title;
      this.url = url;
      this.author = author;
      this.created_at = created_at;
      this.story_id = story_id;
      this.status = status;
    }

    title: string;
    
    url: string;
    
    author: string;
    
    created_at: string;
    
    story_id: number;
    
    status: boolean;
}