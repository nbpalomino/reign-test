import { CreatePostDto } from "./create-post.dto";

export class RawPostsDto {
    readonly hits: CreatePostDto[];
    readonly query: string;
    readonly params: string;
    readonly page: number;
}