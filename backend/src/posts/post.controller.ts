import { Controller, Get, Delete, Param } from '@nestjs/common';
import { PostService } from './post.service';
import { Post } from '../schemas/post.schema'


@Controller('posts')
export class PostController {
  constructor(private readonly postService: PostService) {}

  @Get()
  async getPosts(): Promise<Post[]> {
    return this.postService.findAll();
  }

  @Delete(':id')
  deletePost(@Param() params): Promise<Post> {
    console.log('Deleting post '+params.id);
    return this.postService.delete(params.id);
  }

  @Get('/load')
  async loadPosts(): Promise<any> {
    return this.postService.load();
  }
}
