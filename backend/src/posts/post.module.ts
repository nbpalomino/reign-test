import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PostController } from './post.controller';
import { Post, PostSchema } from '../schemas/post.schema';
import { PostService } from './post.service';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: () => ({
        //uri: `mongodb://${c.get<string>('user')}:${c.get<string>('pass')}@${c.get<string>('server')}/${c.get<string>('database')}`
        uri: `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_SERVER}/${process.env.MONGO_DATABASE}`
      }),
    }),
    MongooseModule.forFeature([{name: Post.name, schema: PostSchema}]),
  ],
  controllers: [PostController],
  providers: [PostService],
})
export class PostModule {}
