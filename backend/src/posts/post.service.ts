import { Model } from 'mongoose'
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Post, PostDocument } from '../schemas/post.schema';
import { CreatePostDto } from '../dto/create-post.dto';
import Axios, { AxiosResponse } from 'axios';
import { RawPostsDto } from '../dto/raw-posts.dto';
import { Cron, CronExpression } from '@nestjs/schedule';

@Injectable()
export class PostService {
  constructor(@InjectModel(Post.name) private postModel: Model<PostDocument>) {}

  async create(createPostDto: CreatePostDto): Promise<any> {
    const createdPost = new this.postModel(createPostDto);

    await createdPost.save((err, p) => {
      if(err) {
        console.log("Error in creating new post: code="+err.code)
        console.log(err.keyValue)
        return err;
      }
      return p;
    })
  }

  async findAll(): Promise<Post[]> {
    return this.postModel.find({status: true}).sort({created_at:-1}).exec()
  }

  async delete(_id: string): Promise<any> {
    return this.postModel.updateOne({ _id }, { status: false }, err => console.log(err));
  }

  @Cron(CronExpression.EVERY_HOUR)
  async load(): Promise<any> {
    console.log("Loading new posts!")
    await Axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .then((res: AxiosResponse<RawPostsDto>) => {
        const posts = res.data.hits

        posts.forEach(p => {
          p.status = true;
          this.create(p);
        })
        return {posts: posts.length}
      })
      .catch(err => console.log(err))
  }

  async prepare(): Promise<any> {

  }
}
