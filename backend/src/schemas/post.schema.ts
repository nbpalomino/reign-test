import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PostDocument = Post & Document

@Schema()
export class Post {
    @Prop()
    title: string;
    @Prop()
    url: string;
    @Prop()
    author: string;
    @Prop()
    created_at: string;
    @Prop()
    story_id: number;
    @Prop()
    status: boolean;
}

export const PostSchema = SchemaFactory.createForClass(Post)
PostSchema.index({title:1, author:1}, {unique:true})