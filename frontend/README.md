Reign Test Frontend
=
*A frontend test code for Reign by Nick B. Palomino*

![image](assets/screenshot.png "Frontend")


Configuration
-
- Run `npm install` or `yarn`
- Run `npm run start`