import React, {Component} from 'react'
import { Provider } from 'react-redux'
import store from './store'
import './styles.scss'

import Banner from './components/Banner'
import Body from './components/Body'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Banner />
        <Body />
      </Provider>
    )
  }
}

export default App