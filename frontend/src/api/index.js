import axios from 'axios'

const SERVER_URL = process.env.API_SERVER || 'http://localhost:3000';

export const deletePost = (_id) => {
  return axios.delete(SERVER_URL+'/posts/'+_id)
    .then(res => res.data)
    .catch(err => console.log(err))
}
export const fetchPosts = () => {
  return axios.get(SERVER_URL+'/posts')
    .then(res => res.data)
    .catch(err => console.log(err))
}