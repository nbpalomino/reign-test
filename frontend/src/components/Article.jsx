import React from 'react'
import { useDispatch } from "react-redux";
import { parseISO, isPast, isYesterday, format } from 'date-fns'

const Article = (props) => {
  const dispatch = useDispatch();

  const deleteArticle = () => {
    props.onDeletePost(props.article._id)
      .then(data => dispatch({type:'remove', payload:props.article._id}))
  }

  const dateFormat = () => {
    let result = 'lost in time';
    let d = parseISO(props.article.created_at)

    if(isPast(d)) {
      result = isYesterday(d) ? 'Yesterday' : format(d, 'MMM dd');
    } else {
      result = format(d, 'HH:mm a')
    }

    return result;
  }

  return (
    <article className={"post"}>
      <a className={"link"} href={props.article.url} target={"_blank"}>
        <div className={"info"}>
          <h3 className={"title"}>{props.article.title}</h3>
          <span className={"author"}>- {props.article.author} -</span>
        </div>
        <div className={"extras"}>
          <span className={"time"} title={parseISO(props.article.created_at)}>{dateFormat(props.article.created_at)}</span>
        </div>
      </a>
      <div className="actions">
        <button className={"btn-delete"} onClick={deleteArticle}><svg xmlns="http://www.w3.org/2000/svg" className={"icon"} viewBox="0 0 20 20" fill="currentColor">
          <path fillRule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clipRule="evenodd" />
        </svg></button>
      </div>
    </article>
  )
}

export default Article