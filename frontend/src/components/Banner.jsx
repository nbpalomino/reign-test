import React, {Component} from 'react'

const Banner = () => {
  return (
    <header id={"banner"}>
      <h1 className={"title"}>HN Feed</h1>
      <p className={"subtitle"}>We &lt;3 hacker news!</p>
    </header>
  )
}

export default Banner