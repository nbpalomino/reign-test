import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Article from './Article'
import { fetchPosts, deletePost } from '../api'

const renderArticles = (articles) => {
  if(articles.length <= 0) return (<h3>Oops, there's no NEWS to read...</h3>)
  return articles.map(art => {
    return (<Article key={art._id} article={art} onDeletePost={deletePost} />);
  })
}

const Body = () => {
  const validArticles = state => {
    console.log(state)
    if(state.articles && state.articles.length) {
      return state.articles.filter(art => {
        if(art.title || art.story_title) {
          art.title ??= art.story_title;
          art.url ??= art.story_url;
          return art;
        }
      })
    }
    return [];
  }
  const dispatch = useDispatch();
  const articles = useSelector(validArticles)

  useEffect(() => {
    fetchPosts()
      .then(data => dispatch({type:'fetch', articles:data}))
  }, [articles.length])

  return (
    <section id={"body"}>
      {renderArticles(articles)}
    </section>
  )
}

export default Body