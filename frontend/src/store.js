import { createStore } from 'redux'

const initialState = {
  articles: []
}

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case 'fetch':
      return {...state, ...rest }
    case 'remove':
      return { articles: state.articles.filter(a => a._id !== rest.payload) }
    default:
      return state
  }
}

export default createStore(changeState)
